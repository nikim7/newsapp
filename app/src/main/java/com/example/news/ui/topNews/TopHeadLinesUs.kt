package com.example.news.ui.topNews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.news.R
import com.example.news.data.db.NewsDatabase
import com.example.news.data.models.Article
import com.example.news.data.network.NewsApi
import com.example.news.data.repositories.NewsRepository
import com.example.news.ui.BaseFragment
import com.example.news.ui.toast
import kotlinx.android.synthetic.main.top_head_lines_us_fragment.*
import kotlinx.coroutines.launch


class TopHeadLinesUs : BaseFragment() {

    companion object {
        fun newInstance() = TopHeadLinesUs()
    }


    private lateinit var factory: TopNewsUsViewModelFactory
    private lateinit var viewModel: TopHeadLinesUsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.top_head_lines_us_fragment, container, false)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setMainScreenServer()
        /*val networkConnection= NetworkConnection(requireContext())
        networkConnection.observe(viewLifecycleOwner, Observer { isConnected ->
            if (isConnected){
                setMainScreenServer()
            }
            else{

            }
        })*/

    }

    private fun setMainScreenServer(){
        val pb:ProgressBar=this.loading_bar
        val api= NewsApi()
        val repository= NewsRepository(api)
        factory=
            TopNewsUsViewModelFactory(repository)
        viewModel=ViewModelProviders.of(this,factory).get(TopHeadLinesUsViewModel::class.java)
        viewModel.getTopNewsUs()
        viewModel.topNewsUs.observe(viewLifecycleOwner, Observer { news ->

           /* for (article in news.articles){
                launch {
                    NewsDatabase(requireContext()).getNewsDbDao().addNews(article)
                }
            }*/
            top_news_us_recycler_view.also {
                it.layoutManager=StaggeredGridLayoutManager(2,LinearLayoutManager.VERTICAL)
                it.setHasFixedSize(true)
                it.adapter =
                    TopNewsRecyclerAdapter(news.articles,pb,requireContext(), object :
                        TopNewsRecyclerAdapter.ViewClickListener{
                        override fun onVClick(article: Article) {
                            //addNews(article)
                            launch {
                                context.let {
                                    NewsDatabase(requireContext()).getNewsDbDao().addNews(article)
                                    if (it != null) {
                                        it.toast("News saved.")
                                    }
                                }
                            }
                        }
                    })
            }
        })
    }
}
