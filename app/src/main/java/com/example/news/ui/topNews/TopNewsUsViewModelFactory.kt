package com.example.news.ui.topNews

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.news.data.repositories.NewsRepository

@Suppress("UNCHECKED_CAST")
class TopNewsUsViewModelFactory(private val repository: NewsRepository)
    : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TopHeadLinesUsViewModel(repository) as T
    }

}