package com.example.news.ui.topNews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.news.R
import com.example.news.data.models.Article
import com.example.news.databinding.TopNewsUsRecyclerItemBinding


class TopNewsRecyclerAdapter(
    private val news: List<Article>,
    private val pb: ProgressBar,
    private val context: Context,
    viewClickListener: ViewClickListener
) : RecyclerView.Adapter<TopNewsRecyclerAdapter.TopNewsViewHolder>() {

    private val listener: ViewClickListener? = viewClickListener

    override fun getItemCount(): Int =news.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopNewsViewHolder =
        TopNewsViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.top_news_us_recycler_item,
            parent,
            false)
    )

    override fun onBindViewHolder(holder: TopNewsViewHolder, position: Int) {
        holder.topNewsUsRecyclerItemBinding.news =news[position]
        pb.visibility= View.GONE

        holder.topNewsUsRecyclerItemBinding.threeDotMenuTextView.setOnClickListener {
            val popupMenu:PopupMenu=PopupMenu(context,it )
            popupMenu.inflate(R.menu.save_options_menu)
            popupMenu.setOnMenuItemClickListener {item ->
                when(item.itemId){
                    R.id.save_news ->{
                        if (listener != null) {
                            listener.onVClick(news[position])
                        }
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }

    inner class TopNewsViewHolder(
        val topNewsUsRecyclerItemBinding: TopNewsUsRecyclerItemBinding
    ) : RecyclerView.ViewHolder(topNewsUsRecyclerItemBinding.root)

    interface ViewClickListener {
        fun onVClick(article: Article)
    }
}

