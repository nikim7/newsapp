package com.example.news.data.repositories

import com.example.news.data.network.NewsApi
import com.example.newsapp.data.repositories.SafeApiRequest

class NewsRepository(
    private val api : NewsApi
    ) : SafeApiRequest() {

    private val apiKey : String= "08b403e85fe94d2d9d3418504f1d1803"

    suspend fun getNewsList()=apiRequest { api.getTopHeadlinesUS("us", apiKey) }

    suspend fun getSourcesList()=apiRequest { api.getSources("en",apiKey) }

    suspend fun getNewsFromSource(sourceId: String?)=apiRequest { api.getNewsFromSource(sourceId,apiKey) }

}